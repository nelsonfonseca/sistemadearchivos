import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Roles } from '../../models/roles.model';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient) { }

  listRoles : Roles[];
  readonly url = "http://localhost:9000/api/v1/roles/rol";

  getRoles() {
    return this.http.get(this.url).toPromise().then(
      response => {
        this.listRoles = response as Roles[];
      }
    );
  }

  postRol(rol : Roles){
    this.getRoles();
    let id = this.listRoles.length +1;
    console.log("id:" + id);
    rol.id = id.toString();
    return this.http.post(this.url, rol);
  }

}
