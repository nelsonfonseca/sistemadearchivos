import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Area } from "../../models/area.model";

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  constructor(private http: HttpClient) { }

  listAreas : Area[];
  readonly url = "http://192.168.0.12:9000/api/v1/areaes/area";

  getAreas() {
    return this.http.get(this.url).toPromise().then(
      response => {
        this.listAreas = response as Area[];
      }
    );
  }

}
