import { Injectable } from '@angular/core';
import { Roles } from '../models/roles.model';
import { DocumentModel } from '../models/document.model';
@Injectable({
  providedIn: 'root'
})
export class DatosService {

    rol : Roles;
    document: DocumentModel;

}