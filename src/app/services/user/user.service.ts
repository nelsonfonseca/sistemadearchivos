import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../../models/user.model';
import { loginModel } from '../../models/login.model';
import { UserLogin } from '../../models/user-login.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  //Variables
  userLoged: UserLogin;
  listUsers: UserModel[];
  readonly url = "http://192.168.0.12:9000/api/v1/authenticatorts";
  url2 = "http://192.168.0.12:9000/api/v1/userses/user";

  //Method to login and get all atributes of user
  userLogin(login: loginModel) {
    return this.http.post(this.url + "/login", login).toPromise().then(
      response => {
        this.userLoged = response as UserLogin;
      }
    );
  }

  //Method to get all users, only to admin
  getUsers() {
    return this.http.get(this.url2).toPromise().then(
      response => {
        this.listUsers = response as UserModel[];
      }
    );
  }

  //Method to edit user, only to admin
  editUser(user: UserModel) {
    return this.http.post(this.url + "/path/", user);
  }

  //Method to delete users, only to admin
  deleteUser(id: number) {
    return this.http.delete(this.url + "/path/" + id);
  }

  //Method to register new users, only to admin
  registerUser(user: UserModel) {
    return this.http.post(this.url2, user);
  }
}
