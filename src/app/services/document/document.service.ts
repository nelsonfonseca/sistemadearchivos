import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DocumentModel } from '../../models/document.model';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  listDocuments : DocumentModel[];
  readonly url = "http://localhost:9000/api/v1/documentes/stock/document";

  constructor(private http: HttpClient) { }

  getDocuments() {
    return this.http.get(this.url).toPromise().then(
      response => {
        this.listDocuments = response as DocumentModel[];
      }
    );
  }

  postDocuments(document : DocumentModel){
    return this.http.post(this.url, document);
  }
}
