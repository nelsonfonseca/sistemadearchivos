import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesFilesComponent } from './modules-files.component';

describe('ModulesFilesComponent', () => {
  let component: ModulesFilesComponent;
  let fixture: ComponentFixture<ModulesFilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModulesFilesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulesFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
