import { Component, OnInit } from '@angular/core';
import { Area} from '../../models/area.model';
import { Roles} from '../../models/roles.model';
import {MatExpansionPanel} from '@angular/material/expansion'
import {MatDialogModule, MatDialog, MatDialogConfig} from '@angular/material/dialog'
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import { Subscription } from "rxjs";
import { DragulaService} from 'ng2-dragula';

@Component({
  selector: 'app-modules-files',
  templateUrl: './modules-files.component.html',
  styleUrls: ['./modules-files.component.css']
})
export class ModulesFilesComponent implements OnInit {
  MANY_ITEMS = 'MANY_ITEMS';
  public  roles = [{

    "id": "1",
    "name": "Empleado1",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false"
  }, {

    "id": "2",
    "name": "Administrador1",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false"
  }, {

    "id": "3",
    "name": "secretarios1",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false"
  }];
  public roles3  = [];
  public roles2 = [{

    "id": "4",
    "name": "Empleado",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false"
  }, {

    "id": "5",
    "name": "Administrador",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false"
  }, {

    "id": "6",
    "name": "secretarios",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false"
  }];
  subs = new Subscription();

  constructor(private dialog: MatDialog, private dragulaService:DragulaService ) {
    this.subs.add(dragulaService.dropModel(this.MANY_ITEMS)
    .subscribe(({ el, target, source, sourceModel, targetModel, item }) => {
      console.log('dropModel:');
      //console.log(el);
      //console.log(source);
      console.log(target.id);
      //console.log(sourceModel);
      console.log(targetModel);
      console.log(item);
      item.name = target.id;
    })
  );
  this.subs.add(dragulaService.removeModel(this.MANY_ITEMS)
    .subscribe(({ el, source, item, sourceModel }) => {
      console.log('removeModel:');
      console.log(el);
      console.log(source);
      console.log(sourceModel);
      console.log(item);
    })
  );
   }

  ngOnInit(): void {
  }


  clickButton(){
    console.log(document.getElementById('lista1').innerHTML);
  }

  /*
  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  
  items = [
    'Carrots',
    'Tomatoes',
    'Onions',
    'Apples',
    'Avocados'
  ];

  basket = [
    'Oranges',
    'Bananas',
    'Cucumbers'
  ];
  drag(event: CdkDragDrop<string>){
    console.log('soy')
    console.log(event.item.moved)
    console.log('fin')
  }
  drop(event: CdkDragDrop<string[]>) {
    console.log('soy')
    console.log(event.item.moved)
    console.log('fin')
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
    }
  }*/
}
