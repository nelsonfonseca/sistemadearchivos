import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $('.sidenav').sidenav();

  }

  //  Variables
  sideNav = false;

  // Methods
  outputinputsidebar(): void {
    this.sideNav = !this.sideNav;
    if (this.sideNav) {
      $('.sidenav').sidenav('open');
    } else {
      $('.sidenav').sidenav('close');
    }



  }


}
