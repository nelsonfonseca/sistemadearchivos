import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
declare var $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(public service : UserService) { }

  ngOnInit(): void {

  }

}
