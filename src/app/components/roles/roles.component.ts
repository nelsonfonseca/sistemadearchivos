import { Component, OnInit } from '@angular/core';
import { AreasService } from "../../services/areas/areas.service";
import { FormGroup, FormControl, FormArray, FormBuilder } from '@angular/forms';
import { RolesService } from "../../services/roles/roles.service";

@Component({
  selector: 'app-rol',
  templateUrl: './roles.component.html'
})
export class RolesComponent implements OnInit {

  constructor(public areasService: AreasService, public rolesService : RolesService) {
    this.rolForm = this.createFormGroup();
   }
  

  ngOnInit(): void {
    this.areasService.getAreas();
  }

  createFormGroup() {
    return new FormGroup({
      id: new FormControl('04'),
      name: new FormControl(''),
      read: new FormControl('false'),
      create: new FormControl('false'),
      update: new FormControl('false'),
      delete: new FormControl('false'),
      area: new FormControl('')
    });
  }

  rolForm: FormGroup;

  onSubmitForm(){
    this.rolesService.postRol(this.rolForm.value).toPromise().then(
      res=>{
        window.alert("Rol registrado exitosamente.");
        this.onResetForm();
      }
    );
  }

  onResetForm() {
    this.rolForm.reset();
  }

}
