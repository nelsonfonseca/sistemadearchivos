import { Component, OnInit } from '@angular/core';
import { DocumentService } from '../../services/document/document.service';
import { DatosService } from "../../services/datos.service";
import { DocumentModel } from 'src/app/models/document.model';
import { ViewFilesComponent } from '../view-files/view-files.component';
import {MatDialogModule, MatDialog, MatDialogConfig} from '@angular/material/dialog'

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  
})
export class DocumentListComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  
  constructor(public documentService : DocumentService,private datosService : DatosService,private dialog: MatDialog) { }
  document: DocumentModel;
  ngOnInit(): void {

    this.documentService.getDocuments();

    this.dtOptions = {
      pagingType: 'full_numbers'
    };
    
  }
  clickDocument(document:DocumentModel){
    this.documentService
    console.log(document);
    this.datosService.document = document;
    this.dialog.open(ViewFilesComponent);

  }

}
