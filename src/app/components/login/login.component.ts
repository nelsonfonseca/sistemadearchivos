import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private service : UserService, private router: Router) { 
    this.loginForm = this.createFormGroup();
  }

  ngOnInit(): void {
  }

  //Model of form
  createFormGroup() {
    return new FormGroup({
      email : new FormControl(''),
      password : new FormControl('')
    });
  }

  loginForm: FormGroup;

  //Variables

  //Methods
  onSubmitForm(){
    this.service.userLogin(this.loginForm.value).then(
      response =>{
        if(this.service.userLoged.status == true){
          this.router.navigate(['/inicio']);
        }else{
          console.log('falso');
          window.alert("Usuario o contraseña errada!");
        }
      }
    );
  }

}
