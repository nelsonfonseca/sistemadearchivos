import { Component, OnInit } from '@angular/core';
import * as DecoupledEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import { DocumentService } from '../../services/document/document.service';
import { DocumentModel } from '../../models/document.model';
import { UserService } from '../../services/user/user.service';

@Component({
	selector: 'app-edit-files',
	templateUrl: './edit-files.component.html',
	styleUrls: ['./edit-files.component.css']
})
export class EditFilesComponent implements OnInit {

	constructor(public documentService : DocumentService, public userService : UserService) { }

	ngOnInit(): void {
		this.documentService.getDocuments();
		this.initEditorToolbar();
		document.getElementById("editor").innerHTML = this.content;
	}

	//Variables
	content = "<p>Hola esto es un ensayo jaja</p>";

	//Methods
	initEditorToolbar() {
		DecoupledEditor
			.create(document.querySelector('.editor'), {
				toolbar: {
					items: [
						'heading',
						'|',
						'fontSize',
						'fontFamily',
						'|',
						'bold',
						'italic',
						'underline',
						'strikethrough',
						'highlight',
						'|',
						'alignment',
						'|',
						'numberedList',
						'bulletedList',
						'|',
						'indent',
						'outdent',
						'|',
						'todoList',
						'link',
						'blockQuote',
						'imageUpload',
						'insertTable',
						'mediaEmbed',
						'|',
						'undo',
						'redo',
						'fontColor',
						'comment',
						'fontBackgroundColor',
						'horizontalLine',
						'CKFinder'
					]
				},
				language: 'es',
				image: {
					toolbar: [
						'imageTextAlternative',
						'imageStyle:full',
						'imageStyle:side'
					]
				},
				table: {
					contentToolbar: [
						'tableColumn',
						'tableRow',
						'mergeTableCells'
					]
				},
				licenseKey: '',
				sidebar: {
					container: document.querySelector('.sidebar')
				},
			})
			.then(editor => {

				// Set a custom container for the toolbar.
				document.querySelector('.document-editor__toolbar').appendChild(editor.ui.view.toolbar.element);
				document.querySelector('.ck-toolbar').classList.add('ck-reset_all');
			})
			.catch(error => {
				console.error(error);
			});

	}

	getInformation() {
		let date = new Date(); 
		let id = this.documentService.listDocuments.length + 1; 
		var documentSend : DocumentModel = {
			"id" : id.toString(),
			"name": "Prueba",
			"version": "1",
			"documento": document.getElementById('editor').innerHTML,
			"createBy": this.userService.userLoged.userName,
			"updateBy": "Carlos",
			"acceptBy": "Maria",
			"publishOn": date.toString()
		};
		
		this.documentService.postDocuments(documentSend).toPromise().then(
			res=>{
			  window.alert("Documento creado exitosamente.");
			}
		  );
	}

}
