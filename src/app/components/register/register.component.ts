import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from '../../services/user/user.service';
import { AreasService } from '../../services/areas/areas.service';
import { RolesService } from '../../services/roles/roles.service';
import { Router } from '@angular/router';
declare var $: any;


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private service: UserService, private router: Router,
    public areasService : AreasService,
    public rolesService : RolesService
    ) { 
    this.registerForm = this.createFormGroup();
  }

  ngOnInit(): void {
    this.areasService.getAreas();
    this.rolesService.getRoles();
  }

  //Model of form
  createFormGroup() {
    return new FormGroup({
      name: new FormControl(''),
      email: new FormControl(''),
      rol: new FormControl(''),
      area: new FormControl(''),
      employeCode: new FormControl(''),
      password: new FormControl('')
    });
  }

  registerForm: FormGroup;

  //Variables

  //Methods
  onSubmitForm() {
    this.service.registerUser(this.registerForm.value).toPromise().then(
      res=>{
        window.alert("Usuario registrado exitosamente.");
        this.onResetForm();
      }
    );
  }

  onResetForm() {
    this.registerForm.reset();
  }

}
