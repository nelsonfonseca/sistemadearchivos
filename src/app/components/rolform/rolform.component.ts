import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatCheckbox} from '@angular/material/checkbox';
import { Roles } from "../../models/roles.model";
import { DatosService } from "../../services/datos.service";

@Component({
  selector: 'app-rolform',
  templateUrl: './rolform.component.html',
  styleUrls: ['./rolform.component.css']
})
export class RolformComponent implements OnInit {

  constructor(private datosService : DatosService) { }

  rol: Roles;
  ngOnInit(): void {
    this.rol = this.datosService.rol;
  }

}
