import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolformComponent } from './rolform.component';

describe('RolformComponent', () => {
  let component: RolformComponent;
  let fixture: ComponentFixture<RolformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
