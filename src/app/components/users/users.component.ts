import { Component, OnInit } from '@angular/core';
import { AreasService } from "../../services/areas/areas.service";
import { RolesService } from "../../services/roles/roles.service";
import { UserService } from "../../services/user/user.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(public areaService: AreasService, public rolesService: RolesService, public userService: UserService) { }

  ngOnInit(): void {
    this.areaService.getAreas();
    this.rolesService.getRoles();
    this.userService.getUsers();
  }

  area = [{
    "name": "Administrativa",
    "description": "Area encargada de la gestión y administración de la empresa"
  }, {
    "name": "Asociativa",
    "description": "Area encargada de la gestión y administración de la empresa"
  }];

  roles = [{

    "id": "1",
    "name": "Empleado",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false",
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }, {

    "id": "2",
    "name": "Administrador",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false",
    "area": {
      "name": "Asociativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }, {

    "id": "3",
    "name": "secretarios",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false",
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }]

  usuarios = [{
    "employeCode": "05",
    "name": "Jhosep Santiago Garcia Aldana",
    "email": "jhosep@gmail.com",
    "password": "jhosep1234",
    "rol": {
      "name": "Empleado",
      "read": "true",
      "create": "false",
      "update": "false",
      "delete": "false",
      "area": {
        "name": "Administrativa",
        "description": "Area encargada de la gestión y administración de la empresa"
      }
    },
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }, {
    "employeCode": "05",
    "name": "Jhosep Santiago Garcia Aldana",
    "email": "jhosep@gmail.com",
    "password": "jhosep1234",
    "rol": {
      "name": "Empleado",
      "read": "true",
      "create": "false",
      "update": "false",
      "delete": "false",
      "area": {
        "name": "Administrativa",
        "description": "Area encargada de la gestión y administración de la empresa"
      }
    },
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }, {
    "employeCode": "05",
    "name": "Jhosep Santiago Garcia Aldana",
    "email": "jhosep@gmail.com",
    "password": "jhosep1234",
    "rol": {
      "name": "Empleado",
      "read": "true",
      "create": "false",
      "update": "false",
      "delete": "false",
      "area": {
        "name": "Administrativa",
        "description": "Area encargada de la gestión y administración de la empresa"
      }
    },
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }, {
    "employeCode": "05",
    "name": "Jhosep Santiago Garcia Aldana",
    "email": "jhosep@gmail.com",
    "password": "jhosep1234",
    "rol": {
      "name": "Empleado",
      "read": "true",
      "create": "false",
      "update": "false",
      "delete": "false",
      "area": {
        "name": "Administrativa",
        "description": "Area encargada de la gestión y administración de la empresa"
      }
    },
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }]

}
