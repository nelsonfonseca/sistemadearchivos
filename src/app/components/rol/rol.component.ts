import { Component, OnInit, ViewChild } from '@angular/core';
import {MatExpansionPanel} from '@angular/material/expansion'
import {MatDialogModule, MatDialog, MatDialogConfig} from '@angular/material/dialog'
import { RolformComponent } from '../rolform/rolform.component';
import { AreasService } from "../../services/areas/areas.service";
import { RolesService } from "../../services/roles/roles.service";
import { Roles } from "../../models/roles.model";
import { DatosService } from "../../services/datos.service";

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  constructor(private dialog: MatDialog, public areaService: AreasService, public rolesService: RolesService, private datosService : DatosService) { }

rols : Roles;

  ngOnInit(): void {
    this.areaService.getAreas();
    this.rolesService.getRoles();
  }
  onDetail(rol : Roles){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "90%";
    //rol = this.child.rol;
    console.log("Rol:");
    console.log(rol);
    this.datosService.rol = rol;
    this.dialog.open(RolformComponent);
  }

  area = [{
    "name": "Administrativa",
    "description": "Area encargada de la gestión y administración de la empresa"
  }, {
    "name": "Asociativa",
    "description": "Area encargada de la gestión y administración de la empresa"
  }];

  roles = [{

    "id": "1",
    "name": "Empleado",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false",
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }, {

    "id": "2",
    "name": "Administrador",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false",
    "area": {
      "name": "Asociativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }, {

    "id": "3",
    "name": "secretarios",
    "read": "true",
    "create": "false",
    "update": "false",
    "delete": "false",
    "area": {
      "name": "Administrativa",
      "description": "Area encargada de la gestión y administración de la empresa"
    }
  }]


}
