import { Component, OnInit } from '@angular/core';
import { DocumentModel } from 'src/app/models/document.model';
import { DatosService } from "../../services/datos.service";

@Component({
  selector: 'app-view-files',
  templateUrl: './view-files.component.html',
  styleUrls: ['./view-files.component.css']
})
export class ViewFilesComponent implements OnInit {

  constructor( private datosService : DatosService) { }
  document: DocumentModel;
  ngOnInit(): void {
    document.getElementById("editor").innerHTML = this.content;
    this.document= this.datosService.document;
  }

  //Variables
  content = "<p>Aqui se podrán observar los documentos sin poder editarlos</p>";




}
