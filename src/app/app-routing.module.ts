import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditFilesComponent } from './components/edit-files/edit-files.component';
import { DocumentListComponent } from './components/document-list/document-list.component';
import { ModulesFilesComponent } from './components/modules-files/modules-files.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { RegisterComponent } from './components/register/register.component';
import { ViewFilesComponent } from './components/view-files/view-files.component';
import {RolComponent} from './components/rol/rol.component';
import { RolesComponent } from './components/roles/roles.component';
import { UsersComponent} from './components/users/users.component';
import { RolformComponent} from './components/rolform/rolform.component'


const routes: Routes = [
  { path : 'inicio', component : PrincipalComponent, children : [
    { path : '', component : EditFilesComponent },
    { path : 'DocumentList', component : DocumentListComponent },
    { path : 'modulesFiles', component : ModulesFilesComponent },
    { path : 'user', component: UserComponent },
    { path : 'registro', component: RegisterComponent },
    { path : 'file', component: ViewFilesComponent },
    { path : 'rol', component: RolComponent },
    { path : 'roles', component: RolesComponent },
    { path : 'users', component: UsersComponent},
    { path : 'rolform', component: RolformComponent}
  ]},
  { path : '', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
