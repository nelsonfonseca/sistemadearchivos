export class DocumentModel {
    id: string;
    name: string;
    version: string;
    documento: string;
    createBy: string;
    updateBy: string;
    acceptBy: string;
    publishOn: string;

}