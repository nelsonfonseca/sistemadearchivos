import { Area } from "./area.model";

export class Roles{
    id : string;
    name : string;
    read : boolean;
    create : boolean;
    update : boolean;
    delete : boolean;
    area : string;
}