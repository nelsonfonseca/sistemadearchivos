import { Roles } from "./roles.model";
import { Area } from "./area.model";

export class UserModel {
    name: string;
    email: string;
    rol: string;
    area: string;
    employeCode: number;
    password: string;
}