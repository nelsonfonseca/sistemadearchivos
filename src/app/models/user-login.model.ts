export class UserLogin{
    userName: string;
    email: string;
    rol: string;
    area: string;
    message: string;
    status: boolean;
}