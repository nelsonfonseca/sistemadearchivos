import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DragulaModule} from 'ng2-dragula';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule} from '@angular/material/dialog'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { EditFilesComponent } from './components/edit-files/edit-files.component';
import { FormsModule } from '@angular/forms';
import { DocumentListComponent } from './components/document-list/document-list.component';
import { ModulesFilesComponent } from './components/modules-files/modules-files.component';
import { UserComponent } from './components/user/user.component';
import { LoginComponent } from './components/login/login.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { FooterComponent } from './components/footer/footer.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { RegisterComponent } from './components/register/register.component';
import { ViewFilesComponent } from './components/view-files/view-files.component';
import { TableModule } from 'primeng/table'
import { DataTablesModule } from 'angular-datatables';
import { DragDropModule} from '@angular/cdk/drag-drop';

//Import of services
import { UserService } from './services/user/user.service';
import { RolComponent } from './components/rol/rol.component';
import { RolesComponent } from './components/roles/roles.component';
import { UsersComponent } from './components/users/users.component';
import { RolformComponent } from './components/rolform/rolform.component';

@NgModule({
  declarations: [
    AppComponent,
    EditFilesComponent,
    DocumentListComponent,
    ModulesFilesComponent,
    UserComponent,
    LoginComponent,
    PrincipalComponent,
    FooterComponent,
    NavbarComponent,
    RegisterComponent,
    ViewFilesComponent,
    RolComponent,
    RolesComponent,
    UsersComponent,
    RolformComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CKEditorModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DragulaModule.forRoot(),
    MatExpansionModule,
    BrowserAnimationsModule,
    MatDialogModule,
    TableModule,
    DataTablesModule,
    DragDropModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent],
  entryComponents:[RolformComponent]
})
export class AppModule { }
